package name.ig.HelloPlayer.instance;

// Tools
import name.ig.HelloPlayer.tools.ControlPanel;
import name.ig.HelloPlayer.tools.PlayList;
import name.ig.HelloPlayer.tools.Settings;

// Assets
import name.ig.HelloPlayer.assets.VideoClip;

public class VideoPlayer {

    private static VideoPlayer instance;
    private VideoClip currentClip;
    private VideoClip prevClip;
    private VideoClip nextClip;

    private PlayList playList;
    private ControlPanel controlPanel;
    private Settings settings;

    public VideoClip getCurrentClip(){
        return currentClip;
    }

    public VideoClip getPrevClip(){
        return prevClip;
    }

    public VideoClip getNextClip(){
        return nextClip;
    }
}
